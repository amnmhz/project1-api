var express= require('express');
var app = express();
var morgan = require('morgan'); //third party middleware
port = 9090;

//var bodyParser = require('body-parser');
var path = require('path');

require('./config/db');

//templating engine setuo
var pug = require('pug');
app.set('view  engine', pug);
app.set('views', 'views-templates'); 

//load router level middleware
var authRouter = require('./controllers/auth.route');
var userRouter = require('./controllers/user.route');
var notificationRouter = require('./controllers/notification.route');
var commentRouter = require('./controllers/comment.route');
var productRouter = require('./controllers/product.route');

//load application level middlewares
var authentication = require('./middlewares/authentication');
var authorization = require('./middlewares/authorization');

//>>2thirdparty middleware
app.use(morgan('dev'));

//inbuilt middleware
// app.use(express.static('files')) locally within express application
// app.use('/file' , express.static('files')) //when other programme (client) 
app.use('/file' , express.static(path.join(__dirname,'files'))) //when other programme (client) 
app.use(express.urlencoded({
    extended: false //will support nested if true else will not
}))
//Using bodyParser instead of express
// app.use(bodyParser.urlencoded({
//     extended: false 
// }))

//app.use(express.json()) 
//or app.use(bodyParser.json());



app.use(function(req,res,next){
    req.aman = 'hello for new req parameter' ; //adding new parameter in request
    next();
}); //app.use require a middleware function 


//app.use('/',authRouter);
//app.use('/auth',checkTicket,checkValidTicket,authRouter);
app.use('/auth',authRouter);
app.use('/about',authRouter);
app.use('/user',authentication,userRouter);
app.use('/product',authentication,authorization,productRouter);
app.use('/comment',authentication,commentRouter);
app.use('/notification',notificationRouter);


app.use(function(req,res,next){

    next({
        msg : 'error 404 not found',
        status : 404
    })
})

///error handling middleware
app.use(function(err,req,res,next){
    console.log('middleware>>', err);
    res.status(err.status || 400);
    res.json({
        message: err.msg || err
    })
})

app.listen(port,function(err,done){
    if(err){
        console.log('failed');
        return;
    }
    console.log('server listening at port',port);
    console.log('enter ctrl+c to exit');
})


//express is web frame work for node
// web is 3 tier application which has 3 layer
// > presentation layer (view)
// > database 
// > controller 

// is a function that has acess to 
// >http request  object,  
// >http response object
// >>middleware can modify http request Object, response Object
// >and acess to next middleware refrence
// ## middleware function most be place in between http request response cycle
//the order of middleware matters

// configuration block to place middleware
// app.use >> app.use is configuration block for middleware

// types of middleware
// 5 types of middleware
// 1 application middleware
// 2 routing middleware
// 3 inbuilt middleware
// 4 third party middleware
// 5 error handling middleware


