var express = require('express');
var router = express.Router();
var Usermodel = require('./../models/user.model');
var passwordHash = require('password-hash');
var UserDataInput = require('../helper/map_user');
var jwt = require('jsonwebtoken');
var config = require('./../config');
var count = 0;

router.get('/login', function (req, res, next) {
    res.render('login.pug', {
        title: 'ExpressLogin'
    });
})

router.post('/login', function (req, res, next) {
    console.log('post request of /login >body> ', req.body);
    //find returns array/// findOne returns object
    Usermodel.findOne({
        username: req.body.username
    })
        .exec(function (err, user) {
            if (err) {
                return next(err);
            }
            //console.log("user>>",user);
            console.log('count >>', count);

            if (user) {
                //console.log(user.status);
                if (user.status == 'active') {
                    //password verifications
                    var isMatched = passwordHash.verify(req.body.password, user.password);
                    if (isMatched) {
                        //generate token
                       // var token = jwt.sign({id:user._id }, config.jwtSecret,{ expiresIn:30}); //expiresIn for token expiry
                        var token = jwt.sign({id:user._id }, config.jwtSecret);
                        res.json({
                            token: token,
                            user: user
                        });
                    }
                    else {
                        if (count > 3) {
                            // Usermodel.status = 'inactive';
                            console.log('user disabled');
                            // console.log(user.status);
                        }
                        next({
                            msg: ' password incorrect login crediential invalid '
                        })
                        count++;
                    }
                }
                else {
                    res.json({
                        message: 'account disabled'
                    })
                }

            }
            else {
                next({
                    msg: ' login crediential invalid  '
                })
            }
        })

})

router.get('/register', function (req, res, next) {
    res.render('register.pug', {
        title: 'ExpressLogin'
    });
})
router.post('/register', function (req, res, next) {
    console.log('post request for register')
    //res.redirect('/auth/login')
    var newUser = new Usermodel({});

    UserDataInput(newUser, req.body);

    // // newUser.save(function (err, user) {
    // //     if (err) {
    // //         return next(err);
    // //     }
    // //     res.json(user);
    // // })

    //using promises
    newUser.save()
        .then(function (data) {
            res.json(data);
        })
        .catch(function (err) {
            res.json(err);
        })
    console.log(req.body)

})

router.get('/about', function (req, res, next) {

    console.log('at get request /about');
    res.status(200);
    res.render('about.pug', {
        title: 'express',
        msg: 'welcome to js express'
    });

})

router.post('/about', function (req, res, next) {

    console.log('at get request /about');
    res.status(200);
    res.render('about.pug', {
        title: 'express',
        msg: 'welcome to js express'
    });

})



module.exports = router;