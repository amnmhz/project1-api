var express = require('express');
var router = express.Router();
var UserModel = require('./../models/user.model');
//var passwordHash = require('password-hash');
var UserDataUpdate = require('../helper/map_user');
var authorize = require('./../middlewares/authorization');



router.get('/', function (req, res, next) {
    //fetch all data from database and respond to user
    //Usermodelfind({1starg_querry},{2nd arrg projection block})
    UserModel.find({}, {
        username: 1,  // projection.. will display only username if username:1 only
        email: 1,
        role: 1  //also display emails.
        // 1 for inclusion and 0 for exclusion
    })
        .sort({
            _id: -1 //sorting decending  last to fst
        })
        // .limit(3)
        // .skip(1) 
        .exec(function (err, user) {
            if (err) {
                return next(err);
            }
            res.json(user);
        })
})

router.get('/:id', function (req, res, next) {
    var id = req.params.id;
    console.log('id >>>', id);
    UserModel.findOne({ _id: id }, function (err, user) {
        if (err) {
            return next(err);
        }
        res.json({ user });
    })
})

router.put('/:id', function (req, res, next) {
    var id = req.params.id;
    console.log('logged in user >>',req.loggedInUser.name); 
    //UserModel.findByIdAndUpdate(id,{for_update},function(err,done))
    //UserModel.update({},{},{})
    UserModel.findById(id).exec(function (err, user) {
        if (err) {
            return next(err);
        }
        if (user) {

            UserDataUpdate(user, req.body);
            user.updatedBy = req.loggedInUser.username;
            //console.log('user>>', user);
            user.save(function (err, updated) {
                if (err) {
                    return next(err);
                }
                res.json(updated);
            })
        }
        else {
            next({
                msg: ' unable to find user'
            })
        }
    })
})

router.delete('/:id',authorize, function (req, res, next) {
    var id = req.params.id;
    UserModel.findById(id, function (err, user) {
        if (err) {
            return next(err);
        }
        if (user) {
            user.remove(function (err, removed) {
                if (err) {
                    return next(err);
                }
                console.log('removed user>>', id);
                res.json({ removed });
            })
        }
        else {
            next({ msg: 'user not found' });
        }
    })
})


module.exports = router;

//benefits of using ODM (mongoose)
//schema based solution
//different data type than js
//method
//middleware
//indexing is easier
