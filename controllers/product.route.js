var productModel = require('./../models/product.model');
var router = require('express').Router();
var map_product = require('./../helper/map_product');
var multer = require('multer');
var fs = require('fs');
var path = require('path');

//diskstorage
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './files/images');
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() + '-' + file.originalname);
    }
});

function imageFilter(req, file, cb) {
    if (file.mimetype.split('/')[0] == 'image') {
        cb(null, true);
    }
    else {
        req.fileTypeError = true;
        cb(null, false);
    }
}

var upload = multer({
    storage: storage,
    fileFilter: imageFilter
})



router.route('/')
    .get(function (req, res, next) {
        productModel.find({}, { user: 1, name: 1, category: 1, brand: 1, image: 1 }) //projection
            .populate('user', { username: 1 }) //projection {user:1,etc}
            .sort({
                _id: -1 //sorting decending  last to fst
            })
            .exec(function (err, products) {
                if (err) {
                    return next(err);
                }
                res.json(products);
                //next();
            })
    })

    .post(upload.single('img'), function (req, res, next) {

        console.log('in files >>>', req.file);
        console.log('in body>>>>>', req.body);

        //filtering image file
        // if (req.file) {
        //     // var mimeType = req.file.mimestype;
        //     console.log('mimeType is >>>', req.file.mimetype.split('/')[0])
        //     if (req.file.mimetype.split('/')[0] != 'image') {
        //         fs.unlink(path.join(process.cwd(), 'files/images/' + req.file.filename), function (err, removed) {
        //             //__dirname for current dir // process.cwd for another dir
        //             if (err) {
        //                 console.log("failed to remove file");
        //             }
        //             else {
        //                 console.log('invalid file removed from db');
        //             }

        //         })
        //         return next({
        //             msg: 'invalid file format'
        //         })
        //     }
        // }
        if (req.fileTypeError) {
            return next({
                msg: 'invalid file format'
            })
        }

        var newProduct = new productModel({});
        map_product(newProduct, req.body); //add product obj map_product
        if (req.file)
            newProduct.image = req.file.filename;
        newProduct.user = req.loggedInUser._id; // also map_product.user = req.loggedInUser._id // mutable property of object//mutation
        newProduct.save(function (err, done) {
            if (err) {
                return next(err);
            }
            res.json(done);
        })

    })


router.route('/search')
    .get(function (req, res, next) {
        var condition = {};
        var searchCondition = map_product(condition, req.query);
        productModel.find(searchCondition)
            .exec(function (err, searched) {
                if (err) {
                    return next(err);
                }
                res.json(searched);
            })
    })

    .post(function (req, res, next) {
        var condition = {};
        var searchCondition = map_product(condition, req.body);
        productModel.find(searchCondition)
            .exec(function (err, searched) {
                if (err) {
                    return next(err);
                }
                res.json(searched);
            })
    })


router.route('/:id')
    .get(function (req, res, next) {
        productModel.findOne({
            _id: req.params.id
        })
            .then(function (data) {
                res.json(data);
            })
            .catch(function (err) {
                next(err);
            });
    })

    .delete(function (req, res, next) {
        productModel.findById(req.params.id)
            .then(function (product) {
                if (product) {
                    product.remove(function (err, removed) {
                        if (err) {
                            return next(err);
                        }
                        res.json(removed);
                    });
                }
                else {
                    next({ msg: 'product not found' });
                }
            })
            .catch(function (err) {
                next(err);
            });
    })

    .put(upload.single('img'), function (req, res, next) {
        productModel.findById(req.params.id)
            .exec(function (err, product) {
                if (err) {
                    return next(err);
                }
                if (product) {
                    var oldImage = product.image;
                    if (req.fileTypeError) {
                        return next({
                            msg: 'invalid file format'
                        })
                    }
                    map_product(product, req.body) // update product
                    product.user = req.loggedInUser._id;
                    if(req.file){
                        product.image = req.file.filename; //adding file name in database
                        //removing old image file
                        fs.unlink(path.join(process.cwd(), 'files/images/' + oldImage))
                    }
                    product.save(function (err, done) {
                        if (err) {
                            return next(err);
                        }
                        res.json(done);
                    })
                }
                else {
                    next({
                        msg: 'product not found'
                    })
                }
            })
    })

module.exports = router;