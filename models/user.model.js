var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var userSchema = new Schema({
    //modelling

    firstName: {
        type: String,
        lowercase: true
        // required: true
    },
    lastName: {
        type: String,
        lowercase: true
        //required: true
    },
    address: {
        type: String,
        // required: true
    },
    email: {
        type: String,
        unique: true,
        lowercase: true,
        sparse: true, //can be update null type
        //required: true
    },
    username: {
        type: String,
        required: true,
        unique: true,
        lowercase: true,
        trim: true //removes space
    },
    password: {
        type: String
    },
    gender: {
        type: String,
        enum: ['male', 'female', 'others']
    },
    status: {
        type: String,
        enum: ['active', 'inactive'],
        default: 'active'
    },
    role: {
        type: Number,
        enum: [1, 2, 3], // 2 for admin
        default: 1
    },
    phoneNumber: {
        type: Number
    },
    eula: {
        type: Boolean,
        default: false
    },
    dob: Date,
    updatedBy: String
}, {
    timestamps: true
});

var userModel = mongoose.model('user', userSchema); //collections >>will set collection to users 

module.exports = userModel;
//or
//module.exports = mongoose.model('user',userSchema);