var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var reviewSchema = new Schema({
    point: {
        type: Number,
        message: String,
        reviewer: String,
        file: String,
        product_id: Number,
        date: Date
    }
})

var productSchema = new Schema({

    name: String,
    category: {
        type: String,
        required: true
    },
    description: String,
    brand: String,
    price: Number,
    color: String,
    modelNo: String,
    warrentyStatus: Boolean,
    warrentyPeriod: String,
    varient: String,
    details: {
        dimension: String,
        weight: Number,
        bodyType: String
    },
    reviews: [reviewSchema],
    user: {
        type: Schema.Types.ObjectId,
        ref: 'user'
    },
    manuDate: Date,
    expDate: Date,
    origin: String,
    image: String,
    tags: [String]

}, {
    timestamps: true
});

var productModel = mongoose.model('product', productSchema); //collection name >> products

module.exports = productModel;