/*
database is a container where we keep data
our database >>> mongodb

//commands:
>mongo and mongod  // mongod is for initializing connection

//connect to data base:
>mongo

//shell commands:
 >show dbs // it will list all the available databases
 
//creating a database :
> use <db_name> // if db_name exist it will select existing database else will create new

>db // to view selected data base
 
//creating a collection as wellas inserting data to existing collection:
>db.<collection_name>.insert({valid json}); //this will create collection as well as insert data to collection

//listing available collection:
>show collections

//fetching from database:
> db.<collection_name>.find({}) // object part i.e.  {} will be query field
//for counting:: 
>db.<collection_name>.find({}).count() // will return total number of items
//viewing in formatted (to prettify)::
>db.<collection_name>.find({}).pretty()  

//to update
>db.<collection_name>.update({},{$set:{data_to_be_updated}}) // 1st object is for query , 2nd object is to update/modify and to add content/data.
//setting_flag is to acess multiple datas, i.e multi:true for all
//upsert:true >>will inserting data in particular collection if not available else won't do nothing
>db.<col_name<.update({},{$set:{data_to_be_updated}},{setting_flag},upsert:true)

//removing:
>db.<collection.name>.remove({})  // will remove everything of that collection if empty object.  Must provide id or <key : value> for removing particular


//dropping collection / removing collection
>db.<collection.name>.drop()

//dropping database:
>db.dropDatabase() // will delete database of (use <database>)


*/

/*
#####   Backup and Restore  #####

can be done in two ways 
1. bson structure (bson= binary json // unreadble format)
2. json and csv 

1> bson
command :-
mongodump // will backup bson data into default dump folder
mongodump --db <db-name> //selected database into dumpfolder
mongodump --db <db-name> --out /<dirtory or path_to_dir>  //database will be store in that path

restore:
mongorestore //this will take backup data from dump folder
mongorestore <path_to_bson_backup_folder> // will back up from that directory

2>json format

mongoexport --db <db-nmae> --collection <col_name> --out path_to dir_with_.jsonfile
mongoexport -d <db-nmae> -c <col_name> -o path_to_dir_with_.jsonfile

restore::
mongoimport --db <db_name> --collection <collection_name> path_to_dir_with .json file

3>csv format
export:
mongoexport --db <db_name> --collection <col_name> --type=csv --fields 'comma_seperated_headers' --out destination_path_with csv extention
eg: mongoexport -d <db_name> -c <col_name> --type=csv --fields 'username,email,location' -o /asd/sdf.csv

import:
mongoimport -d <db_new> -c <col_nmae_new> --type=csv path_to csv_file --headerline