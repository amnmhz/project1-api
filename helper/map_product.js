module.exports = function map_product(PRODUCT, reqBody) {
    if (reqBody.name)
        PRODUCT.name = reqBody.name;
    if (reqBody.category)
        PRODUCT.category = reqBody.category;
    if (reqBody.brand)
        PRODUCT.brand = reqBody.brand;
    if (reqBody.description)
        PRODUCT.description = reqBody.description;
    if (reqBody.price)
        PRODUCT.price = reqBody.price;
    if (reqBody.color)
        PRODUCT.color = reqBody.color;
    if (reqBody.weight)
        PRODUCT.details.weight = reqBody.weight;
    if (reqBody.bodyType)
        PRODUCT.details.bodyType = reqBody.bodyType;
    if (reqBody.dimension)
        PRODUCT.details.dimension = reqBody.dimension;
    if (reqBody.origin)
        PRODUCT.origin = reqBody.origin;
    if (reqBody.manufactureDate)
        PRODUCT.manuDate = reqBody.manufactureDate;
    if (reqBody.expiryDate)
        PRODUCT.expDate = reqBody.expiryDate;
    if (reqBody.warrentyStatus)
        PRODUCT.warrentyStatus = reqBody.warrentyStatus;
    if (reqBody.warrentyPeriod)
        PRODUCT.warrentyPeriod = reqBody.warrentyPeriod;
    if (reqBody.tags) {
        PRODUCT.tags = reqBody.tags.split(',');
    }  
    return PRODUCT;
}