var passwordHash = require('password-hash');

var USERDATA = function(USER,reqBody) {
//user data input/modify
    if (reqBody.firstname)
        USER.firstName = reqBody.firstname;
    if (reqBody.lastname)
        USER.lastName = reqBody.lastname;
    if (reqBody.address)
        USER.address = reqBody.address;
    if (reqBody.emailAddress)
        USER.email = reqBody.emailAddress;
    if (reqBody.username)
        USER.username = reqBody.username;
    if (reqBody.password)
        USER.password = passwordHash.generate(reqBody.password);
    if (reqBody.date_of_birth)
        USER.dob = reqBody.date_of_birth;
    if (reqBody.gender)
        USER.gender = reqBody.gender;
    if (reqBody.phoneNumber)
        USER.phoneNumber = reqBody.phoneNumber;
    if (reqBody.role)
        USER.role = reqBody.role;
    if (reqBody.eula)
        USER.eula = reqBody.eula;
    
    return USER;
}

module.exports = USERDATA;
